
Django doesn't support mysql in python 3 so i used postgresql instead

The pipeline should look like the following:


Non master branches:

    Build docker images -> Test image -> Stage -> Deploy
	docker-compose	->  curl -> Tag images -> push to gcr.io


Master branch:

    Build Docker Images -> Tag Image as latest -> uplaod to gcr.io -> Update kubernetes pods



The architecture should look like this:

	(Port 80) Nginx (Scalable)  ------->  Django App Server (8000) -----> Membase Cache ---> Postgresql DB
								       -----> Redis
									
